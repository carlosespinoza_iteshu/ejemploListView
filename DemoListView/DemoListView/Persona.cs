﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoListView
{
    public class Persona
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string DeporteFavorito { get; set; }
        public string UrlFoto { get; set; }
        public override string ToString()
        {
            return $"{Nombre} {Apellidos}";
        }
    }
}
