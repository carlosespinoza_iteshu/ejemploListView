﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace DemoListView
{
    public class Repositorio
    {
        private List<Persona> directorio;
        public Repositorio()
        {
            directorio = new List<Persona>()
            {
                new Persona()
                {
                    Id=Guid.NewGuid().ToString(),
                     Nombre="Juan",
                     Apellidos="Perez",
                     FechaNacimiento=new DateTime(1980,05,14),
                     DeporteFavorito="Futbol",
                     UrlFoto="https://www.redlg.net/buscar_personas/buscar/fotos/peque/YtNp7kUEmrS.jpeg"
                },
                new Persona()
                {
                    Id=Guid.NewGuid().ToString(),
                     Nombre="Maria",
                     Apellidos="Martinez",
                     FechaNacimiento=new DateTime(1985,05,11),
                     DeporteFavorito="Basketball",
                     UrlFoto="https://www.redlg.net/buscar_personas/buscar/fotos/peque/uDBwWRy2APwD.jpeg"
                },
                new Persona()
                {
                    Id=Guid.NewGuid().ToString(),
                     Nombre="Mario",
                     Apellidos="Morales",
                     FechaNacimiento=new DateTime(1990,05,14),
                     DeporteFavorito="Baseball",
                     UrlFoto="https://tnc2014.terena.org/includes/tnc2014/gfx/photos/people/BrianN.jpg "
                },
                new Persona()
                {
                    Id=Guid.NewGuid().ToString(),
                     Nombre="Lupita",
                     Apellidos="Hernandez",
                     FechaNacimiento=new DateTime(1980,02,14),
                     DeporteFavorito="",
                     UrlFoto="http://kashmer.org/wp-content/uploads/2018/08/israh.png"
                }
            };
        }
        public List<Persona> Read { get=>directorio;  }

        public Persona Create(Persona p)
        {
            p.Id = Guid.NewGuid().ToString();
            directorio.Add(p);
            return p;
        }
        public Persona Update (Persona p)
        {
            Persona update = directorio.Find(e => e.Id == p.Id);
            update.Nombre = p.Nombre;
            update.Apellidos = p.Apellidos;
            update.FechaNacimiento = p.FechaNacimiento;
            update.DeporteFavorito = p.DeporteFavorito;
            update.UrlFoto = p.UrlFoto;
            return update;
        }
        public bool Delete(Persona p)
        {
            directorio.Remove(p);
            return true;
        }
        
       
     }
}
